import {Injectable} from '@angular/core';
import {API} from "aws-amplify";
import {AmplifyService} from 'aws-amplify-angular';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AppService {


  public readonly authorised$ = new BehaviorSubject(false);
  public readonly predictors$ = new BehaviorSubject([]);

  private headers;

  constructor(private amplifyService: AmplifyService) {
    amplifyService.authStateChange$.subscribe(a => {
      console.log(a)
      this.authorised$.next(!!a.user)
      if (a.user) {
        this.headers = this.buildHeaders(a);
        this.loadData();
      }
    })
  }

  private buildHeaders(a) {
    return {Authorization: `Bearer ${a.user.signInUserSession.accessToken.jwtToken}`};
  }

  loadData() {
    API.get('gateway', '/api/v1/predictors', {headers: this.headers}).then(x => {
      console.log(x);
      setTimeout(() => {
        this.predictors$.next(x);
      }, 10)
    })
  }

  initializePrediction(request): Promise<{ predictionId: string }> {
    return API.post('gateway', '/api/v1/predictions', {
        headers: this.headers,
        body: request
      }
    )
  }

  getPredictionResult(predictionId): Promise<{ predictionId: string }> {
    return API.get('gateway', `/api/v1/predictions/${predictionId}/result`, {
        headers: this.headers
      }
    )
  }
}
