import {ChangeDetectorRef, Component} from '@angular/core';
import {AppService} from "./app.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  authorized = false;

  constructor(private appService: AppService, private ref: ChangeDetectorRef) {
    appService.authorised$.subscribe(a => {
      setTimeout(() => {
        this.authorized = a;
        this.ref.detectChanges();
      });
    })
  }
}
