import {ChangeDetectorRef, Component} from '@angular/core';
import {AppService} from "../app.service";
import {AbstractControl, FormControl, FormGroup, ValidatorFn} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  predictors = []
  dataSetNames = []
  algorithmNames = []

  dataSet = null;
  algorithm = null;
  data = null;
  result = {};

  form = new FormGroup({
    dataSetName: new FormControl(null),
    algorithmName: new FormControl(null),
    data: new FormControl(null, [this.dataValidator()])
  });

  constructor(private appService: AppService, private ref: ChangeDetectorRef) {
    appService.predictors$.subscribe(p => {
      setTimeout(() => {
        this.predictors = p;
        this.dataSetNames = [...new Set(p.map(x => x.dataSetName))];
        this.ref.detectChanges();
      });
    })
  }

  dataSetSelected(e) {
    this.dataSet = e.value;
    this.algorithmNames = [...new Set(this.predictors.filter(x => x.dataSetName == e.value).map(x => x.algorithmName))];
  }

  algorithmSelected(e) {
    this.algorithm = e.value;
  }

  dataValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const v = control.value
      try {
        this.parseData(v)
      } catch (e) {
        return {'invalidData': {value: control.value}}
      }
      return null;
    };
  }

  parseData(value: string) {
    return value.replace(/(^\s*,)|(,\s*$)/g, '').split(',').map(x => {
        const n = Number(x)
        if (Number.isNaN(n)) {
          throw 'Nan in data array'
        }
        return n
      }
    )
  }

  predict() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.result = {message: 'Waiting...'};
      const v = this.form.value
      const predictor = this.predictors.find(p => p.dataSetName == v.dataSetName && p.algorithmName == v.algorithmName)
      const request = {
        predictorId: predictor.id,
        data: {
          values: this.parseData(v.data)
        }
      }
      this.appService.initializePrediction(request).then(p => {
        const predictionId = p.predictionId;
        this.getPredictionResult(predictionId);
      })
    }
  }

  getPredictionResult(predictionId: string, attempt = 0) {
    this.appService.getPredictionResult(predictionId).then(
      result => {
        console.log(predictionId, result, attempt);
        if (result) {
          this.result = result;
        } else if (attempt < 5) {
          setTimeout(() => {
            this.getPredictionResult(predictionId, ++attempt)
          }, 1000);
        } else {
          this.result = {message: 'Prediction timeout'};
        }
      }
    )
  }
}
