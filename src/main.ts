import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

import {Amplify, API, Hub, Auth} from 'aws-amplify';
import awsconfig from './aws-exports';


if (environment.production) {
  enableProdMode();
}

Amplify.configure(awsconfig);
API.configure({
  endpoints: [
    {
      name: "gateway",
      endpoint: environment.endpoint
    }
  ]
})


platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
